local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

IAENV.AddPrefabPostInit("boat", function(inst)
    if TheWorld.ismastersim and TheWorld:HasTag("island") then
        inst:AddComponent("mapwrapper")
    end
end)