local assets =
{
  Asset("ANIM", "anim/seashell.zip"),
}

local function on_hammer(inst, hammerer, workleft, workdone)
  local num_shells_worked = math.clamp(math.ceil(workdone / TUNING.SEASHELL_HAMMERS), 1, inst.components.stackable:StackSize())

  local loot_data = TUNING.SEASHELL_LOOT

  -- Generate a list of prefabs to create first and optimize the loop by having every type here.
  local spawned_prefabs = {
      ["slurtle_shellpieces"] = 0,
  }

  for _ = 1, num_shells_worked do
    spawned_prefabs["slurtle_shellpieces"] = spawned_prefabs["slurtle_shellpieces"] + 1
  end

  -- Then create these prefabs while stacking them up as much as they are able to.
  for prefab, count in pairs(spawned_prefabs) do
      local i = 1
      while i <= count do
          local loot = SpawnPrefab(prefab)
          local room = loot.components.stackable ~= nil and loot.components.stackable:RoomLeft() or 0
          if room > 0 then
              local stacksize = math.min(count - i, room) + 1
              loot.components.stackable:SetStackSize(stacksize)
              i = i + stacksize
          else
              i = i + 1
          end
          LaunchAt(loot, inst, hammerer, loot_data.SPEED, loot_data.HEIGHT, nil, loot_data.ANGLE)
      end
  end

  -- Play custom sound
  if hammerer.SoundEmitter then
    hammerer.SoundEmitter:PlaySound("dontstarve/common/destroy_pot")
  end

  -- Finally, remove the actual stack items we just consumed
  local top_stack_item = inst.components.stackable:Get(num_shells_worked)
  top_stack_item:Remove()
end

local function OnExplosion_seashell_full(inst, data)
  local hammerer = data and data.explosive or nil
  if hammerer then
      local loot_data = TUNING.SEASHELL_LOOT
      LaunchAt(inst, inst, hammerer, loot_data.SPEED, loot_data.HEIGHT, nil, loot_data.ANGLE)
  end
end

local function stack_size_changed(inst, data)
  if data ~= nil and data.stacksize ~= nil and inst.components.workable ~= nil then
      inst.components.workable:SetWorkLeft(data.stacksize * TUNING.SEASHELL_HAMMERS)
  end
end

local function fn()
  local inst = CreateEntity()
  inst.entity:AddTransform()
  inst.entity:AddAnimState()
  inst.entity:AddSoundEmitter()
  inst.entity:AddNetwork()

  MakeInventoryPhysics(inst)

  inst.AnimState:SetBank("seashell")
  inst.AnimState:SetBuild("seashell")
  inst.AnimState:PlayAnimation("idle")

  inst.pickupsound = "rock"

  inst:AddTag("molebait")

    MakeInventoryFloatable(inst)
    inst.components.floater:UpdateAnimations("idle_water", "idle")

    inst.entity:SetPristine()

  if not TheWorld.ismastersim then
    return inst
  end

    inst:AddComponent("inventoryitem")

  inst:AddComponent("stackable")
  inst.components.stackable.maxsize = TUNING.STACK_SIZE_MEDITEM

  inst:AddComponent("tradable")

  inst:AddComponent("workable")
  inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
  inst.components.workable:SetWorkLeft(TUNING.SEASHELL_HAMMERS * inst.components.stackable.stacksize)
  inst.components.workable:SetOnWorkCallback(on_hammer)
  inst.components.workable.savestate = false
  
  inst:AddComponent("inspectable")

  inst:AddComponent("edible")
  inst.components.edible.foodtype = FOODTYPE.ELEMENTAL
  inst.components.edible.healthvalue = 1

  inst:AddComponent("bait")

  inst:AddComponent("repairer")
  inst.components.repairer.repairmaterial = MATERIALS.SHELL
  inst.components.repairer.healthrepairvalue = TUNING.REPAIR_SHELL_HEALTH

  -- The amount of work needs to be updated whenever the size of the stack changes
  inst:ListenForEvent("stacksizechange", stack_size_changed)

  -- Explosions knock around these shells in specific.
  inst:ListenForEvent("explosion", OnExplosion_seashell_full)

  MakeHauntableLaunch(inst)
  MakeBlowInHurricane(inst, TUNING.WINDBLOWN_SCALE_MIN.LIGHT, TUNING.WINDBLOWN_SCALE_MAX.LIGHT)

  return inst
end

return Prefab("seashell", fn, assets)
