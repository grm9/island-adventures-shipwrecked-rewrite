local assets =
{
	Asset("ANIM", "anim/trap_sea.zip"),
}

local function onharvested(inst)
    if inst.components.finiteuses then
        inst.components.finiteuses:Use(1)
    end
end

local function onbaited(inst, bait)
	inst:PushEvent("baited")
	bait:Hide()
end

local function onpickup(inst, doer)
	if inst.components.trap and inst.components.trap.bait and doer.components.inventory then
		inst.components.trap.bait:Show()
		inst.components.trap.bait:DoTaskInTime(0, function(bait)
            doer.components.inventory:GiveItem(bait)
        end) -- makes it so you get the bait after the trap
	end

    inst.components.trap:Reset() -- prevented the trap from listening to the pickup event and automatically resetting the trap before this function gets triggered so need to reset
    -- the trap manually through this function
end

local function OnInit(inst)
    if not IsOnOcean(inst) then
        inst.AnimState:PlayAnimation("idle")
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
    inst.Physics:SetShouldPassGround(false)

    inst.MiniMapEntity:SetIcon("rabbittrap.png")

	inst.AnimState:SetBank("trap_sea")
	inst.AnimState:SetBuild("trap_sea")
	inst.AnimState:PlayAnimation("idle")

    inst:AddTag("trap")
	
	inst.no_wet_prefix = true

	MakeInventoryFloatable(inst)
	inst.components.floater:UpdateAnimations("idle_water", "idle")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.nobounce = true
    inst.components.inventoryitem:SetOnPickupFn(onpickup)

	inst:AddComponent("finiteuses")
	inst.components.finiteuses:SetMaxUses(TUNING.TRAP_USES)
	inst.components.finiteuses:SetUses(TUNING.TRAP_USES)
	inst.components.finiteuses:SetOnFinished(inst.Remove)

    inst:AddComponent("trap")
    inst.components.trap.targettag = "lobster"
    inst.components.trap:SetOnHarvestFn(onharvested)
    inst.components.trap:SetOnBaitedFn(onbaited)
    -- inst.components.trap.baitsortorder = 1 --not needed, we hide the bait anyways
	inst.components.trap.range = 2
    inst.components.trap.water = true
    local OnPickup = UpvalueHacker.GetUpvalue(inst.components.trap.OnRemoveFromEntity, "OnPickup") -- can't put it inside the function because it returns more than 1 value and the
    -- second value gets counted as the third argument for the function and causes a crash because it's a number but the third argument is supposed to be an entity
    inst:RemoveEventCallback("onpickup", OnPickup) -- to prevent the bait from getting removed from the
    -- component before the onpickup function that gives the bait to the one who picked the trap up gets triggered

    inst:DoTaskInTime(0, OnInit)

    MakeHauntableLaunch(inst)

    inst:SetStateGraph("SGseatrap")

    return inst
end

return Prefab("seatrap", fn, assets)
